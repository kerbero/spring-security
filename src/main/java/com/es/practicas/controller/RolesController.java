package com.es.practicas.controller;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class RolesController {

	@GetMapping("/acces-admin")
	//@PreAuthorize("hasRole('ADMIN')")
	@PreAuthorize("hasAnyRole('ADMIN','USER')")
	public String accessAdmin() {return "Hola Eres el administrador!";}
	
	@GetMapping("/acces-user")
	@PreAuthorize("hasRole('USER')")
	public String accessUser() {return "Hola Eres el Usuario!";}
	
	@GetMapping("/acces-invited")
	@PreAuthorize("hasRole('INVITED')")
	public String accessInvited() {return "Hola Eres el invitado!";}
}
